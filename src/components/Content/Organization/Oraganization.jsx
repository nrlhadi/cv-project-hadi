import React from "react";

const Oraganization = () => {
  return (
    <ul>
      <h2>Doya Collection 88 August 2020 - Present</h2>
      <h2>Business Owner</h2>
      <p>A muslim fashion who focused on selling Abaya Turkey, available in offline ( Tanah Abang Blok F 4th Floor ) and online like Lazada and Tokopedia</p>
      <p>1. Manage online store and offline store</p>
      <p>2. Already has sale a product more than 1500 items</p>
      <p>3. Managed Customer, and Created a Report for cash flow</p>
      <p>4. Create Purchase orders for Local vendors</p>
      <p>5. Creating a website to expand branding for increase sales</p>


        <h2>PT. Integra Loyalti Nusantara (Goodie.id) June 2019 – September 2020</h2>
        <h2>Front-end web developer</h2>
        <p>B2B Digital Loyalty Platform who provides an easy-to-use provides various loyalty algorithms to give the best experience to users, also provides flexible loyalty point redemption or usage-based Tebet, South Jakarta</p>
        <p>1. Developed and implemented functioning and smooth front-end web using React.Js</p>
        <p>2. Interacts front-end web application with RESTful API services.</p>
        <p>3. Evaluates and implementing the front-end web-based on the UI/UX design.</p>
        <p>4. Modified existing software to correct errors and optimize efficiency.</p>
        <p>5. Testing Fuctionality.</p>
        <p>6. Modified code to fix errors.</p>


        <h2>PT. Media Baru Internasional (MoGawe) February 2019 – Mei 2019</h2>
        <h2>Junior Software Developer</h2>
        <p>MoGawe is a mobile crowdsourcing application that can help companies/businesses to get part-time workers (#MoGawers) throughout Indonesia as it has now started from survey data collection, field inspection, mystery shopper, asset monitoring, Customer traffic generator, and more others by using a mobile application so that the results can be received faster, more accurately at af ordable costs. based Tebet, South Jakarta.</p>
        <p>1. Developed and implemented software solutions based on product requirements</p>
        <p>2. Modified existing software to correct errors and optimize efficiency</p>
        <p>3. fixing bugs</p>

    </ul>

  );
};

export default Oraganization;
