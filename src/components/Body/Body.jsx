import React from "react";
import Card from "../Card/Card";
import Projects from "../../Services/projects.json";
import Educations from "../../Services/educations.json";
import "./style.css";
import Education from "../Content/Education/Education";
import Oraganization from "../Content/Organization/Oraganization";
import Html5Icon from "../../assets/html5.svg";
import WebDesignIcon from "../../assets/web-design.svg";
import RestApiIcon from "../../assets/rest-api.svg";
import PostmanIcon from "../../assets/postman.svg";
import VsIcon from "../../assets/vscode.svg";
import WebstormIcon from "../../assets/webstorm.svg";
import JsIcon from "../../assets/js.svg";
import CssIcon from "../../assets/css.svg";
import  ReactIcon from "../../assets/react.svg"
import GmailIcon from "../../assets/gmail.svg"
import InstagramIcon from "../../assets/instagram.svg";
import FacebookIcon from "../../assets/facebook.svg";
import GitlabIcon from "../../assets/gitlab.svg";
import GitIcon from "../../assets/git.svg";
import LinkedIcon from "../../assets/linked.svg"


const Body = () => {
  return (
    <div className="layout">
      <aside className="body__container">
        <div>
          <h1>Work</h1>
          <Card {...Projects[0]} />
          <Card {...Projects[1]} />
          <Card {...Projects[2]} />
          <Card {...Projects[3]} />
        </div>
      </aside>
      <main className="body__container">
        <div className="body__content">
          <h1 style={{fontSize: "2.5em"}}>Programming Language And Tools</h1>
          <img style={{width: "60px", paddingRight: "5px"}} src={Html5Icon} alt="html5" />
          <img style={{width: "60px", paddingRight: "5px"}} src={CssIcon} alt="css" />
          <img style={{width: "60px", paddingRight: "5px"}} src={JsIcon} alt="js" />
          <img style={{width: "60px", paddingRight: "5px"}} src={ReactIcon} alt="react" />
          <img style={{width: "60px", paddingRight: "5px"}} src={WebDesignIcon} alt="webDesign" />
          <img style={{width: "60px", paddingRight: "5px"}} src={WebstormIcon} alt="webstorm" />
          <img style={{width: "60px", paddingRight: "5px"}} src={VsIcon} alt="vs" />
          <img style={{width: "60px", paddingRight: "5px"}} src={PostmanIcon} alt="postman" />
          <img style={{width: "60px", paddingRight: "5px"}} src={RestApiIcon} alt="restApi" />
          {/*<Education {...Educations[0]} />*/}
          {/*<Education {...Educations[1]} />*/}

          <h1>Experience</h1>
          <Oraganization />

          <h1>Contact</h1>
          <ul style={{ fontSize: "14pt" }}>
            <p>Feel free to get in touch regarding anything tech-related or not. I usually respond within 48 hours, but you may need to wait a bit longer if I'm really busy.</p>
            <div style={{justifyContent: "center"}} className="profile-link">
              <img style={{width: "50px", paddingRight: "5px"}} src={GmailIcon} alt="gmail" />
            <a style={{color: "black"}}
                href={`mailto: nrlhadi24@gmail.com`}
                target="_blank"
                rel="noopener noreferrer"
            >nrlhadi24@gmail.com
            </a>
            </div>`
            <div style={{justifyContent: "center"}} className="profile-link">
              <a style={{color: "black"}}
                 href="https://github.com/NurulHadi24"
                 target="_blank"
                 rel="noopener noreferrer"
              >
                <img style={{width: "30px", paddingRight: "5px"}} src={GitIcon} alt="gmail" />
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a style={{color: "black"}}
                 href="https://www.linkedin.com/in/nurul-hadi-568817178/"
                 target="_blank"
                 rel="noopener noreferrer"
              >
                <img style={{width: "30px", paddingRight: "5px"}} src={LinkedIcon} alt="gmail" />
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a style={{color: "black"}}
                 href="https://www.instagram.com/nrlhd24/"
                 target="_blank"
                 rel="noopener noreferrer"
              >
                <img style={{width: "30px", paddingRight: "5px"}} src={InstagramIcon} alt="gmail" />
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a style={{color: "black"}}
                 href="https://www.facebook.com/nurul.hadi.378"
                 target="_blank"
                 rel="noopener noreferrer"
              >
                <img style={{width: "30px", paddingRight: "5px"}} src={FacebookIcon} alt="gmail" />
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a style={{color: "black"}}
                 href="https://gitlab.com/nrlhadi"
                 target="_blank"
                 rel="noopener noreferrer"
              >
                <img style={{width: "30px", paddingRight: "5px"}} src={GitlabIcon} alt="gmail" />
              </a>
            </div>`
          </ul>
        </div>
      </main>
    </div>
  );
};

export default Body;
