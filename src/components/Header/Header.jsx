import React from "react";
import "./style.css";
import GithubIcon from "../../assets/github.svg";
import LinkedIcon from "../../assets/linkedin.svg";
import InstagramIcon from "../../assets/instagram.svg";
import FacebookIcon from "../../assets/facebook.svg";


const Header = () => {
  return (
    <header className="center">
      <div className="container">
        <div className="profile-picture">
          <img crossOrigin="anonymous" src="hadi-profile.jpeg" loading="eager" />
        </div>
        <div className="profile-summary">
          <h1>Nurul Hadi</h1>
          <hr />
          {/*<div className="profile-stats">*/}
          {/*  <span>*/}
          {/*    <b>Age:</b> 26*/}
          {/*  </span>*/}
          {/*  <span>*/}
          {/*    <b>E-mail:</b> nrlhadi24@gmail.com*/}
          {/*  </span>*/}
          {/*</div>*/}
          <div className="profile-bio">
            Hallo there,
            Hadi is a person who has good analytical thinking, problem-solving, fast-learner and always bring my best self to work either works as a team or individuals. Have graduated from Computer Science Major in Information System, I have an interest in any field related IT industry such as Mobile Development, Software Quality Assurance, and Product/Project Management
            {/*<div className="profile-link">*/}
            {/*  <a*/}
            {/*    href="https://github.com/NurulHadi24/"*/}
            {/*    target="_blank"*/}
            {/*    rel="noopener noreferrer"*/}
            {/*  >*/}
            {/*    <img src={GithubIcon} alt="github" />*/}
            {/*  </a>*/}
            {/*  &nbsp;&nbsp;&nbsp;&nbsp;*/}
            {/*  <a*/}
            {/*    href="https://www.linkedin.com/in/nurul-hadi-568817178/"*/}
            {/*    target="_blank"*/}
            {/*    rel="noopener noreferrer"*/}
            {/*  >*/}
            {/*    <img src={LinkedIcon} alt="linkedin" style={{ maxWidth: 30 }} />*/}
            {/*  </a>*/}
            {/*  <a*/}
            {/*      href="https://www.instagram.com/nrlhd24/"*/}
            {/*      target="_blank"*/}
            {/*      rel="noopener noreferrer"*/}
            {/*  >*/}
            {/*    <img src={InstagramIcon} alt="instagram" style={{ maxWidth: 30 }} />*/}
            {/*  </a>*/}
            {/*  <a*/}
            {/*      href="https://www.facebook.com/nurul.hadi.378"*/}
            {/*      target="_blank"*/}
            {/*      rel="noopener noreferrer"*/}
            {/*  >*/}
            {/*    <img src={FacebookIcon} alt="facebook" style={{ maxWidth: 30 }} />*/}
            {/*  </a>*/}
            {/*</div>*/}
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
